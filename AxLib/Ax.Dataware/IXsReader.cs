﻿// ---   Copyright   ----------------------------------------------------------
//  This file is part of the AxLibrary.
//
//  AxLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  any later version.

//  AxLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with AxLibrary. If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------
using System;
using System.IO;

namespace Ax.Dataware
{
    public interface IXsReader : IDisposable
    {
        XsLoadOptions Options { get; }
        Stream Stream { get; }

        object ReadValue(XsNodeType type);
        string ReadName();
        string ReadHeader();
        void ReadFooter();
        XsNodeType ReadType();
        bool ReadElementEnd();
    }
}
