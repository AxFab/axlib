﻿// ---   Copyright   ----------------------------------------------------------
//  This file is part of the AxLibrary.
//
//  AxLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  any later version.

//  AxLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with AxLibrary. If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------
using System;
using System.IO;

namespace Ax.Dataware
{
    public interface IXsWriter : IDisposable
    {
        XsSaveOptions Options { get; }
        Stream Stream { get; }
        void StartDocument(string name);
        void EndDocument(string name);
        void WriteStartElement(string name);
        void WriteEndElement(string name);
        void WriteStartCollection(string name, int count);
        void WriteEndCollection(string name);
        void WriteStartTable(string name, int count);
        void WriteEndTable(string name);
        void WriteElementAs(string Name, object item, XsNodeType type);
        void WriteValueAs(object item, XsNodeType type);
        void WriteSeparator();
    }
}
