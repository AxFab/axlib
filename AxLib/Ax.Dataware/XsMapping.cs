﻿// ---   Copyright   ----------------------------------------------------------
//  This file is part of the AxLibrary.
//
//  AxLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  any later version.

//  AxLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with AxLibrary. If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------
using System;
using System.Reflection;

namespace Ax.Dataware
{
    public class XsMapping
    {
        public XsSchemaType Schema { get; private set; }
        public string Name { get; private set; }
        public object DefaultValue { get; internal set; }
        public bool IsConst { get; internal set; }
        public int MaxOccurs { get; internal set; }
        public int MinOccurs { get; internal set; }
        public bool Nullable { get { return MinOccurs == 0; } }
        public bool Unbound { get { return MaxOccurs == -1; } }
        public bool Unique { get { return MaxOccurs == 1; } }

        private PropertyInfo Property { get; set; }

        // Build Index(SearchIndex, UniqueKey, ForeignKey, PrimaryKey ...)

        public override string ToString()
        {
            return Name + "  : " + Schema.Name + "(" + Schema.NodeType + ")";
        }

        public XsMapping(string name, XsSchemaType schema)
        {
            this.Name = name;
            this.Schema = schema;
            if (schema != null)
            {
                if (schema.NodeType == XsNodeType.Array)
                {
                    this.MinOccurs = 0;
                    this.MaxOccurs = -1;
                }
                else if (schema.NodeType == XsNodeType.Collection || schema.NodeType == XsNodeType.Table)
                {
                    this.MinOccurs = 1;
                    this.MaxOccurs = 1;
                }
                else
                {
                    this.MinOccurs = 0;
                    this.MaxOccurs = 1;
                }
            }
        }

        public object GetValue(object item)
        {
            return this.Property.GetValue(item, null);
        }

        public void SetValue(object item, object value)
        {
            this.Property.SetValue(item, Convert.ChangeType(value, this.Schema.SystemType), null);
        }

        public static XsMapping FromMember(PropertyInfo property)
        {
            XsSchemaType schem = XsSchemaType.FromType(property.PropertyType);
            object[] attr = property.GetCustomAttributes(typeof(XsSerializableAttribute), false);
            string name = (attr.Length != 0 ? (attr[0] as XsSerializableAttribute).Name : property.Name);
            return new XsMapping(name, schem)
            {
                Property = property,
            };
        }
    }

}
