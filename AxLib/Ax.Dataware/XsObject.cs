﻿// ---   Copyright   ----------------------------------------------------------
//  This file is part of the AxLibrary.
//
//  AxLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  any later version.

//  AxLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with AxLibrary. If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Ax.Dataware
{
    public class XsObject
    {
        public XsObject(string name, params object[] items)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            this.Name = name;
            this.Add(items);
        }

        public string Name { get; set; }
        public XsNodeType NodeType { get; set; }
        public object Value { get; set; }
        public XsObject Parent { get; set; }
        public XsObject NextSibling { get; set; }
        public XsObject PreviousSibling { get; set; }
        public XsObject FirstChild { get; set; }
        public XsObject LastChild { get; set; }

        // TODO assign default value
        private static Type writerType;
        public static Type WriterType
        {
            get
            {
                return writerType;
            }
            set
            {
                if (!typeof(IXsWriter).IsAssignableFrom(value))
                    throw new ArgumentException("value doesn't implement 'IXsWriter'");
                // Search for constructor (Stream, XsLoadOption)
                writerType = value;
            }
        }

        // TODO assign default value
        private static Type readerType;
        public static Type ReaderType
        {
            get
            {
                return readerType;
            }
            set
            {
                if (!typeof(IXsReader).IsAssignableFrom(value))
                    throw new ArgumentException("value doesn't implement 'IXsReader'");
                // Search for constructor (Stream, XsLoadOption)
                readerType = value;
            }
        }

        public XsDocument Document
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool HasElement
        {
            get
            {
                return this.FirstChild != null;
            }
        }

        public void Add(params object[] items)
        {
            XsObject firstNew = Link(this, items);
            if (firstNew == null)
            {
                return;
            }

            XsObject newLast = firstNew.PreviousSibling;
            if (LastChild != null)
                LastChild.NextSibling = firstNew;
            else
                FirstChild = firstNew;
            firstNew.PreviousSibling = LastChild;
            LastChild = newLast;
        }

        public void AddAfterSelf(params object[] items)
        {
            XsObject firstNew = Link(this.Parent, items);
            if (firstNew == null)
            {
                return;
            }

            if (this.NextSibling != null)
            {
                firstNew.PreviousSibling.NextSibling = this.NextSibling;
                this.NextSibling.PreviousSibling = firstNew.PreviousSibling;
            }
            else
            {
                // firstNew.PreviousSibling.NextSibling = null;
                this.Parent.LastChild = firstNew.PreviousSibling;
            }

            this.NextSibling = firstNew;
            firstNew.PreviousSibling = this;
        }

        public void AddBeforeSelf(params object[] items)
        {
            XsObject firstNew = Link(this.Parent, items);
            if (firstNew == null)
            {
                return;
            }

            XsObject newPrev = firstNew.PreviousSibling;
            if (this.PreviousSibling != null)
            {
                this.PreviousSibling.NextSibling = firstNew;
                firstNew.PreviousSibling = this.PreviousSibling;
            }
            else
            {
                this.Parent.FirstChild = firstNew;
                firstNew.PreviousSibling = null;
            }

            this.PreviousSibling = newPrev;
            newPrev.NextSibling = this;
        }

        public void AddFirst(params object[] items)
        {
            XsObject firstNew = Link(this, items);
            if (firstNew == null)
            {
                return;
            }

            firstNew.PreviousSibling.NextSibling = FirstChild;
            firstNew.PreviousSibling = null;
            if (FirstChild == null)
                LastChild = firstNew;
            else
                FirstChild.PreviousSibling = firstNew;
            FirstChild = firstNew;
        }

        public XsObject Element(string name)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            XsObject child = this.FirstChild;
            while (child != null)
            {
                if (child.Name == name)
                    return child;
                child = child.NextSibling;
            }

            return null;
        }

        public IEnumerable<XsObject> Elements()
        {
            XsObject child = this.FirstChild;
            while (child != null)
            {
                yield return child;
                child = child.NextSibling;
            }
        }

        public IEnumerable<XsObject> Elements(string name)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            XsObject child = this.FirstChild;
            while (child != null)
            {
                if (child.Name == name)
                    yield return child;
                child = child.NextSibling;
            }
        }

        public void Remove()
        {
            if (this.Parent == null)
                return;

            if (this.PreviousSibling != null)
                this.PreviousSibling.NextSibling = this.NextSibling;
            else
                this.Parent.FirstChild = this.NextSibling;

            if (this.NextSibling != null)
                this.NextSibling.PreviousSibling = this.PreviousSibling;
            else
                this.Parent.LastChild = this.PreviousSibling;

            this.Parent = null;
        }

        public void RemoveElements()
        {
            XsObject child = this.FirstChild;
            while (child != null)
            {
                XsObject ws = child;
                child = child.NextSibling;
                ws.Parent = null;
                ws.PreviousSibling = null;
                ws.NextSibling = null;
            }

            this.FirstChild = null;
            this.LastChild = null;
        }

        public void Save(Stream stream)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            using (IXsWriter writer = Activator.CreateInstance(WriterType, stream) as IXsWriter)
            {
                this.Save(writer);
            }
        }

        public void Save(string uri)
        {
            if (uri == null)
                throw new ArgumentNullException("uri");
            using (Stream stream = File.Open(uri, FileMode.Create, FileAccess.Write))
            using (IXsWriter writer = Activator.CreateInstance(WriterType, stream) as IXsWriter)
            {
                this.Save(writer);
            }
        }

        private XsObject Link(XsObject owner, params object[] items)
        {
            if (owner == null)
            {
                throw new InvalidOperationException();
            }

            if (items == null || items.Length == 0)
                return null;
                
            XsObject first = null;
            XsObject last = null;
            XsObject obj;
            foreach (object item in items)
            {
                if (item is XsObject)
                {
                    obj = item as XsObject;
                }
                else
                {
                    if (owner.Value == null)
                    {
                        owner.Value = items[0];
                        continue;
                    }
                    else
                        throw new NotSupportedException();
                }

                obj.Parent = owner;
                if (first == null)
                {
                    first = obj;
                    last = obj;
                }
                else
                {
                    last.NextSibling = obj;
                    obj.PreviousSibling = last;
                    last = obj;
                }
            }

            if (first == null)
                return null;

            first.PreviousSibling = last;
            return first;
        }

        public void Save(IXsWriter writer)
        {
            writer.StartDocument(null);
            WriteIt(writer);
            writer.EndDocument(null);
        }

        private void WriteIt(IXsWriter writer) 
        {
            if (this.HasElement)
            {
                writer.WriteStartElement(this.Name);
                XsObject c = this.FirstChild;
                while (c != null)
                {
                    if (c != this.FirstChild)
                        writer.WriteSeparator();
                    c.WriteIt(writer);
                    c = c.NextSibling;
                }

                writer.WriteEndElement(this.Name);
            }
            else if (this.Value != null)
            {
                writer.WriteElementAs(this.Name, this.Value, this.NodeType);
            }
            else
            {
                writer.WriteElementAs(this.Name, null, XsNodeType.Null);
            }
        }

        public override string ToString()
        {
            using (MemoryStream stream = new MemoryStream())
            using (IXsWriter writer = Activator.CreateInstance(WriterType, stream) as IXsWriter)
            {
                writer.StartDocument(null);
                WriteIt(writer);
                writer.EndDocument(null);
                stream.Position = 0;
                byte[] tmp = new byte[stream.Length];
                stream.Read(tmp, 0, (int)stream.Length);
                return Encoding.UTF8.GetString(tmp);
            }
        }

        #region [   Static Methods   ]

        public static int CompareDocumentOrder(XsObject x1, XsObject x2)
        {
            throw new NotImplementedException();
        }

        public static bool DeepEquals(XsObject x1, XsObject x2)
        {
            if (x1 == null)
                return x2 == null;
            if (x2 == null)
                return false;
            //if (x1.NodeType
            if (x1.Name != x2.Name)
                return false;
            if (x1.Value != null)
            {
                return x1.Value.Equals(x2.Value);
            }
            if (x2.Value != null)
                return false;


            IEnumerator<XsObject> ei1 = x1.Elements().GetEnumerator();
            foreach (XsObject e2 in x2.Elements())
            {
                if (!ei1.MoveNext() || !DeepEquals(ei1.Current, e2))
                {
                    return false;
                }
            }

            return !ei1.MoveNext();
        }

        public static XsObject Load(string uri)
        {
            if (uri == null)
                throw new ArgumentNullException("uri");
            using (Stream stream = File.OpenRead(uri))
            using (IXsReader reader = Activator.CreateInstance(ReaderType, stream) as IXsReader)
            {
                return Load(reader);
            }
        }

        public static XsObject Load(Stream stream)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");
            using (IXsReader reader = Activator.CreateInstance(ReaderType, stream) as IXsReader)
            {
                return Load(reader);
            }
        }

        public static XsObject Load(IXsReader reader)
        {
            if (reader == null)
                throw new ArgumentNullException("reader");
            return ReadIt(reader);
        }



        public static XsObject ReadIt(IXsReader reader)
        {
            throw new NotImplementedException();
        }

        public static XsObject Parse(string text)
        {
            throw new NotImplementedException();
            /*
            if (string.IsNullOrEmpty(text))
                throw new ArgumentException("text");
            using (TextStream stream = TextStream.FromString(text))
            using (IXsReader reader = Activator.CreateInstance(ReaderType, stream) as IXsReader)
            {
                return Load(reader);
            }*/
        }

        #endregion [   Static Methods   ]

        public override bool Equals(object obj)
        {
            XsObject o2 = obj as XsObject;
            if (o2 == null)
                return false;
            return DeepEquals(this, o2);
        }

        public static XsDiffer operator -(XsObject objA, XsObject objB)
        {
            throw new NotImplementedException();
        }
    }


    // EXperimental
    enum XsDiffState
    {
        Both, Add, Remove, Identical, ValueDiffer, ChildDiffer
    }

    public class XsDiffer
    {
        public string Name { get; private set; } // Name must be identical 
        public int state { get; private set; } // +1 0 ou -1
        public object ValueA { get; private set; }
        public object ValueB { get; private set; }
        public XsDiffer Parent { get; private set; }
        public XsDiffer PreviousSibling { get; private set; }
        public XsDiffer NextSibling { get; private set; }
        public XsDiffer FirstChild { get; private set; }
        public XsDiffer NextChild { get; private set; }
        
    }
}
