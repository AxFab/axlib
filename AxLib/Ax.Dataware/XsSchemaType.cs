﻿// ---   Copyright   ----------------------------------------------------------
//  This file is part of the AxLibrary.
//
//  AxLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  any later version.

//  AxLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with AxLibrary. If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Ax.Dataware
{
    public class XsSchemaType
    {
        public string Name { get; private set; }
        public XsNodeType NodeType { get; private set; }
        public Type SystemType { get; private set; }
        public bool IsAbstract { get; private set; }
        public bool IsMixed { get; private set; }
        public List<string> Restrictions;
        public List<XsSchemaType> Extentions;
        public List<XsMapping> Content = new List<XsMapping>();

        public static XsSchemaType FromType(Type type)
        {
            XsSchemaType scty = new XsSchemaType();
            object[] attr = type.GetCustomAttributes(typeof(XsSerializableAttribute), false);
            scty.Name = (attr.Length != 0 ? (attr[0] as XsSerializableAttribute).Name : type.Name);
            scty.NodeType = GetNodeType(type);
            scty.IsAbstract = false;
            scty.IsMixed = false;
            scty.SystemType = type;
            if (scty.NodeType == XsNodeType.Object)
            {
                foreach (PropertyInfo info in type.GetProperties())
                {
                    scty.Content.Add(XsMapping.FromMember(info));
                }
            }
            else if (!IsPrimitive(scty.NodeType))
            {
                // Basic Array use separator and can be save without naming all element
                // Amf3 & Bem use both as same, but not Xml
                // An array we take the sub element
                // An collection and table are element
                // Collection may differs with options
                throw new NotImplementedException();
            }

            return scty;
        }

        public static bool IsPrimitive(XsNodeType type)
        {
            return type < XsNodeType.Array;
        }

        public static XsNodeType GetNodeType(Type type)
        {
            if (type.BaseType == typeof(ValueType))
            {
                if (type == typeof(Int16) || type == typeof(UInt16)
                    || type == typeof(Int32) || type == typeof(UInt32)
                    || type == typeof(SByte) || type == typeof(Byte)
                    || type == typeof(Int64))
                    return XsNodeType.Integer;
                else if (type == typeof(DateTime))
                    return XsNodeType.Date;
                else if (type == typeof(Guid))
                    return XsNodeType.Guid;
                throw new NotImplementedException();
            }
            else
            {
                if (type == typeof(string))
                    return XsNodeType.String;
                else if (type.IsSubclassOf(typeof(IEnumerable<>)))
                    return XsNodeType.Array;
                return XsNodeType.Object;
                throw new NotImplementedException();
            }

        }

        public XsMapping SearchChild(string name)
        {
            foreach (XsMapping mmap in this.Content)
            {
                if (mmap.Name == name)
                {
                    return mmap;
                }
            }

            return null;
        }

        public object Deserialize(IXsReader reader)
        {
            // HERE I ALREADY KNOW THAT I AM ON THIS OBJECT
            // Xml <Object (xstype="System.sd")
            // Json "Object": ({"xstype":"System.sd",)
            // Bem  0x06 Object

            // ON VIENT DE LIRE LE NOM, ON DOIT DEMANDER SI IL CONNAIT LE TYPE MAINTENANT
            // JSON PEUT REPONDRE  Object, Array, ou Unknown

            // On vient de lire le nom, on verifie avec le type
            XsNodeType type = reader.ReadType();

            if (IsPrimitive(this.NodeType))
            {
                return reader.ReadValue(this.NodeType);
            }
            else
            {
                object item = Activator.CreateInstance(this.SystemType);
                while (true)
                {
                    string name = reader.ReadName();
                    if (name == null)
                    {
                        // The object doesnt have sub element yet !?
                        throw new Exception("What's happend");
                    }

                    XsMapping mmap = this.SearchChild(name);
                    if (mmap == null) // (Warning unknow member)
                    {
                        continue;
                    }

                    mmap.SetValue(item, mmap.Schema.Deserialize(reader));

                    if (reader.ReadElementEnd())
                    {
                        break;
                    }
                }

                return item;
            }

            throw new NotImplementedException();
        }

        public void Serialize(IXsWriter writer, object item)
        {
            if (IsPrimitive(this.NodeType))
            {
                writer.WriteElementAs(this.Name, item, this.NodeType);
            }
            else
            {
                bool first = true;
                foreach (XsMapping xmp in this.Content)
                {
                    if (first) first = false;
                    else writer.WriteSeparator();

                    if (IsPrimitive(xmp.Schema.NodeType))
                    {
                        writer.WriteElementAs(xmp.Name, xmp.GetValue(item), xmp.Schema.NodeType);
                    }
                    else if (xmp.Schema.NodeType == XsNodeType.Object)
                    {
                        writer.WriteStartElement(xmp.Name);
                        xmp.Schema.Serialize(writer, xmp.GetValue(item));
                        writer.WriteEndElement(xmp.Name);
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                }
            }

        }
    }

}
