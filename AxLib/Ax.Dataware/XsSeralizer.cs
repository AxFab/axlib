﻿// ---   Copyright   ----------------------------------------------------------
//  This file is part of the AxLibrary.
//
//  AxLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  any later version.

//  AxLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with AxLibrary. If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------
using System;
using System.IO;
using System.Text;

namespace Ax.Dataware
{
    public class XsSeralizer
    {
        public Type writerType;
        public Type readerType;

        public XsSeralizer()
        {
            this.LoadOptions = new XsLoadOptions();
            this.SaveOptions = new XsSaveOptions();
            // The type need to come from the app.Settings

            // TODO assign default value
            this.WriterType = null;
            this.ReaderType = null;
        }

        public XsLoadOptions LoadOptions { get; set; }

        public XsSaveOptions SaveOptions { get; set; }

        public Type WriterType
        {
            get
            {
                return writerType;
            }
            set
            {
                // Search for constructor (Stream, XsSaveOption)
                if (!typeof(IXsWriter).IsAssignableFrom(value))
                    throw new ArgumentException("value doesn't implement 'IXsWriter'");
                writerType = value;
            }
        }

        public Type ReaderType
        {
            get
            {
                return readerType;
            }
            set
            {
                if (!typeof(IXsReader).IsAssignableFrom(value))
                    throw new ArgumentException("value doesn't implement 'IXsReader'");
                // Search for constructor (Stream, XsLoadOption)
                readerType = value;
            }
        }

        public void Serialize(string uri, object item)
        {
            using (Stream stream = File.Open(uri, FileMode.Create, FileAccess.Write))
            using (IXsWriter writer =
                Activator.CreateInstance(WriterType, stream, SaveOptions) as IXsWriter)
            {
                this.Serialize(writer, item);
            }
        }

        public void Serialize(Stream stream, object item)
        {
            using (IXsWriter writer =
                Activator.CreateInstance(WriterType, stream, SaveOptions) as IXsWriter)
            {
                this.Serialize(writer, item);
            }
        }

        public void Serialize(IXsWriter writer, object item)
        {
            XsSchemaType mapping = XsSchemaType.FromType(item.GetType());
            writer.StartDocument(mapping.Name);
            mapping.Serialize(writer, item);
            writer.EndDocument(mapping.Name);
        }

        public string Stringify(object item) 
        {
            using (MemoryStream stream = new MemoryStream())
            using (IXsWriter writer = 
                Activator.CreateInstance(WriterType, stream, SaveOptions) as IXsWriter)
            {
                this.Serialize(writer, item);
                stream.Position = 0;
                byte[] tmp = new byte[stream.Length];
                stream.Read(tmp, 0, (int)stream.Length);
                return Encoding.UTF8.GetString(tmp);
            }
        }

        public object Deserialize(IXsReader reader, Type type)
        {
            XsSchemaType mapping = XsSchemaType.FromType(type);
            string name = reader.ReadHeader();
            if (mapping.Name != name)
            {
                throw new Exception("Wrong object");
            }
            object item = mapping.Deserialize(reader);
            reader.ReadFooter();
            return item;
        }

        public object Deserialize(string uri, Type type)
        {
            using (Stream stream = File.OpenRead(uri))
            using (IXsReader reader =
                Activator.CreateInstance(ReaderType, stream, LoadOptions) as IXsReader)
            {
                return Deserialize(reader, type);
            }
        }

        public object Deserialize(Stream stream, Type type)
        {
            using (IXsReader reader =
                Activator.CreateInstance(ReaderType, stream, LoadOptions) as IXsReader)
            {
                return Deserialize(reader, type);
            }
        }

        public object Parse(string text, Type type)
        {
            using (MemoryStream stream = new MemoryStream())
            using (IXsReader reader =
                Activator.CreateInstance(ReaderType, stream, LoadOptions) as IXsReader)
            {
                byte[] tmp = Encoding.UTF8.GetBytes(text);
                stream.Write(tmp, 0, tmp.Length);
                stream.Position = 0;
                return this.Deserialize(reader, type);
            }
        }

    }

}
