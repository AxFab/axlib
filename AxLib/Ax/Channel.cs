﻿// ---   Copyright   ----------------------------------------------------------
//  This file is part of the AxLibrary.
//
//  AxLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  any later version.

//  AxLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with AxLibrary. If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Ax
{
    public delegate string LogFormater(LogRecord record);

    public delegate byte[] LogBinaryFormater(LogRecord record);

    public class Channel : IDisposable
    {
        internal int Refs = 0;
        private Stream Output;
        private TextWriter Writer;
        public LogFormater Formater;
        public LogBinaryFormater BinaryFormater;
        public LogLevel Levels;

        public Channel(Stream output, LogLevel level)
        {
            this.Output = output;
            this.Levels = level;
            this.BinaryFormater = UTF8Formater;
            this.Formater = FileFormat;
        }

        public Channel(string uri, LogLevel level)
            : this(File.Open(uri, FileMode.OpenOrCreate, FileAccess.Write), level)
        {
        }

        public Channel(TextWriter writer, LogLevel level)
        {
            this.Output = null;
            this.Writer = writer;
            this.Levels = level;
            this.BinaryFormater = UTF8Formater;
            this.Formater = ConsoleFormat;
        }

        public void Dispose()
        {
            if (Output != null)
            {
                Output.Dispose();
            }
            else
            {
                Writer.Dispose();
            }
        }

        public IEnumerable<LogRecord> LoadRange(DateTime start, DateTime end)
        {
            throw new NotImplementedException();
        }

        public void Log(LogRecord log)
        {
            if (Output != null)
            {
                byte[] bytes = BinaryFormater(log);
                Output.Write(bytes, 0, bytes.Length);
            }
            else
            {
                Writer.WriteLine(Formater(log));
            }
        }

        public byte[] UTF8Formater(LogRecord record)
        {
            return Encoding.UTF8.GetBytes(this.Formater(record) + "\n");
        }

        public static string ConsoleFormat(LogRecord record)
        {
            return string.Format("{2} {4}.{3} :: {0}", record.Message, record.Timestamp, record.Level, record.Caller.GetMethod().Name, record.Caller.GetMethod().ReflectedType.Name);
        }

        public static string FileFormat(LogRecord record)
        {
            return string.Format("{1} - {2} {4}.{3} :: {0}", record.Message, record.Timestamp, record.Level, record.Caller.GetMethod().Name, record.Caller.GetMethod().ReflectedType.Name);
        }
    }
}
