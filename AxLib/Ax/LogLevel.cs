﻿// ---   Copyright   ----------------------------------------------------------
//  This file is part of the AxLibrary.
//
//  AxLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  any later version.

//  AxLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with AxLibrary. If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------
using System;

namespace Ax
{
    [Flags]
    public enum LogLevel
    {
        Trace = 0x1,
        Debug = 0x2,
        Info = 0x4,
        Warning = 0x8,
        Error = 0x10,
        Critical = 0x20,

        DebugOrMore = LogLevel.Debug | LogLevel.InfoOrMore,
        InfoOrMore = LogLevel.Info | LogLevel.WarningOrMore,
        WarningOrMore = LogLevel.Warning | LogLevel.ErrorOrMore,
        ErrorOrMore = LogLevel.Error | LogLevel.Critical,

        Alls = LogLevel.Trace | LogLevel.DebugOrMore,
    }
}
