﻿// ---   Copyright   ----------------------------------------------------------
//  This file is part of the AxLibrary.
//
//  AxLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  any later version.

//  AxLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with AxLibrary. If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------
using System;
using System.Diagnostics;

namespace Ax
{
    public struct LogRecord
    {
        public LogRecord(LogLevel level, DateTime time, StackTrace stackTrace, string message, params object[] args)
        {
            if (Number.SingleFlag((long)level) < 0)
                throw new ArgumentException("level");

            this.Level = level;
            this.Timestamp = time;
            this.Message = string.Format(message, args);
            this.Caller = stackTrace.GetFrame(0);
        }

        public LogRecord(LogLevel level, string message, params object[] args)
            : this(level, DateTime.Now, new StackTrace(1), message, args)
        {
        }

        public string Message;
        public LogLevel Level;
        public DateTime Timestamp;
        public StackFrame Caller;
    }
}
