﻿// ---   Copyright   ----------------------------------------------------------
//  This file is part of the AxLibrary.
//
//  AxLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  any later version.

//  AxLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with AxLibrary. If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;

namespace Ax
{
    public class Logger : IDisposable
    {
        public static Logger Main { get; private set; }
        private static Dictionary<string, Logger> Loggers = new Dictionary<string, Logger>();
        private Dictionary<string, Channel> Channels = new Dictionary<string, Channel>();

        static Logger ()
        {
            Main = new Logger();
            Loggers.Add("main", Main);
            Main.AddChannel ("console", new Channel(Console.Out, LogLevel.Alls));
        }

        private Logger()
        {
        }

        public static Logger GetLogger(string name)
        {
            Logger logger;
            if (Loggers.TryGetValue(name, out logger))
            {
                return logger;
            }

            logger = new Logger();
            Loggers.Add(name, logger);
            return logger;
        }

        public void Log(LogRecord log)
        {
            foreach (Channel ch in Channels.Values)
            {
                if ((ch.Levels & log.Level) != 0)
                {
                    ch.Log(log);
                }
            }
        }

        public void Critical(string message, params object[] args)
        {
            this.Log(new LogRecord(LogLevel.Critical, DateTime.Now, new StackTrace(1), message, args));
        }

        
        public void Error(string message, params object[] args)
        {
            this.Log(new LogRecord(LogLevel.Error, DateTime.Now, new StackTrace(1), message, args));
        }

        public void Warning(string message, params object[] args)
        {
            this.Log(new LogRecord(LogLevel.Warning, DateTime.Now, new StackTrace(1), message, args));
        }

        public void Info(string message, params object[] args)
        {
            this.Log(new LogRecord(LogLevel.Info, DateTime.Now, new StackTrace(1), message, args));
        }

        public void Trace(string message, params object[] args)
        {
            this.Log(new LogRecord(LogLevel.Trace, DateTime.Now, new StackTrace(1), message, args));
        }

        public void Debug(string message, params object[] args)
        {
            this.Log(new LogRecord(LogLevel.Debug, DateTime.Now, new StackTrace(1), message, args));
        }
        
        public void AddChannel(string name, Channel channel)
        {
            Channels.Add (name, channel);
            channel.Refs++;
        }
        
        public Channel GetChannel(string name)
        {
            Channel channel;
            if (Channels.TryGetValue(name, out channel))
            {
                return channel;
            }

            throw new Exception("channel doesn't exist");
        }

        public IEnumerable<Channel> GetChannels()
        {
            return Channels.Values;
        }

        public void RemoveChannel (string name) 
        {
            Channel channel;
            if (!Channels.TryGetValue(name, out channel))
                throw new InvalidOperationException("channel doesn't exist");
            Channels.Remove (name);
            channel.Refs--;
            if (channel.Refs <= 0)
                channel.Dispose();
        }

        public void Dispose()
        {
            foreach (Channel channel in Channels.Values)
            {
                channel.Refs--;
                if (channel.Refs <= 0)
                    channel.Dispose();
            }

            Channels.Clear();
        }
    }
}

