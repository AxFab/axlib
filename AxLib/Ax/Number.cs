﻿// ---   Copyright   ----------------------------------------------------------
//  This file is part of the AxLibrary.
//
//  AxLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  any later version.

//  AxLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with AxLibrary. If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ax
{
    public class Number
    {
        public static int Align(int value, int limit)
        {
            return (value + limit - 1) & ~(limit - 1);
        }

        public static int SingleFlag(long value)
        {
            int k = 0;
            while (value != 0)
            {
                if ((value & 0x1) == 1)
                {
                    return (value == 1 ? k : -1);
                }
                value = value >> 1;
                ++k;
            }
            return -1;
        }

        public static string MemorySize(ulong size)
        {
            string[] preffixes = new string[] { 
                "bytes",
                "Kb",
                "Mb",
                "Gb",
                "Tb",
                "Pb",
                "Eb",
                "Zb",
                "Yb",
            };

            ulong bits = size;
            ulong rest = 0;
            int idx = 0;
            while ((bits & ~0x3FFUL) != 0)
            {
                rest = bits & 0x3FFUL;
                bits = bits >> 10;
                ++idx;
            }

            if (idx == 0 && bits < 1000)
                return string.Format("{0:0} {2}", bits, rest, preffixes[idx]);
            else if (bits < 10)
                return string.Format("{0:0}.{1:00} {2}", bits, rest * 100 / 1024, preffixes[idx]);
            else if (bits < 100)
                return string.Format("{0:00}.{1:0} {2}", bits, rest * 10 / 1024, preffixes[idx]);
            else if (bits < 1000)
                return string.Format("{0:000} {2}", bits, rest * 1 / 1024, preffixes[idx]);
            else
                return string.Format("0.{1:00} {2}", bits, (1024 + rest) * 10 / 1024, preffixes[idx + 1]);
        }
    }
}
