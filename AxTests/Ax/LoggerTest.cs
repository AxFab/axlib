﻿// ---   Copyright   ----------------------------------------------------------
//  This file is part of the AxLibrary.
//
//  AxLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  any later version.

//  AxLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with AxLibrary. If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------
using System;
using System.IO;

using NUnit.Framework;

namespace Ax
{
    [TestFixture]
    class LoggerTest
    {
        [Test]
        public void TestBasic ()
        {
            using (MemoryStream mem = new MemoryStream())
            {
                Logger log = Logger.GetLogger("main");
                log.RemoveChannel("console");
                Channel chnlw = new Channel(mem, LogLevel.Trace | LogLevel.Debug);
                log.AddChannel("test_lw", chnlw);
                Channel chnhg = new Channel(mem, LogLevel.WarningOrMore);
                log.AddChannel("test_hg", chnhg);

                log.Info("First info log at {0}", new DateTime());
                log.Trace("No !");
                log.Error("Oh big fail...");
                log.Warning("Carefull...");

                Channel chnfile = new Channel("logs.tmp", LogLevel.Trace | LogLevel.Debug);
                using (Logger log2 = Logger.GetLogger("test2"))
                {
                    log2.AddChannel("test_lw", chnlw);
                    log2.AddChannel("test_file", chnfile);
                    log2.Debug("Just to see if it's work");
                    log2.Critical("FATAL !!!");
                }

                Assert.AreEqual(log.GetChannel("test_hg"), chnhg, "#1");
                Assert.Throws<Exception>(() =>
                {
                    log.GetChannel("console");
                }, "#2");

                foreach (Channel ch in log.GetChannels())
                {
                }

                Assert.Throws<ArgumentException>(() =>
                {
                    new LogRecord(LogLevel.InfoOrMore, "No way...");
                }, "#3");

                Assert.Throws<InvalidOperationException>(() =>
                {
                    log.RemoveChannel("thing");
                }, "#3");


                new LogRecord(LogLevel.Info, "Should work...");

                File.Delete("logs.tmp");
            }
        }
    }
}
