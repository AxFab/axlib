﻿// ---   Copyright   ----------------------------------------------------------
//  This file is part of the AxLibrary.
//
//  AxLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  any later version.

//  AxLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.

//  You should have received a copy of the GNU Lesser General Public License
//  along with AxLibrary. If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Ax
{
    [TestFixture]
    class NumberTest
    {
        [Test]
        public void TestSize()
        {
            Assert.AreEqual("835 bytes", Number.MemorySize(0x343), "#1");
            Assert.AreEqual("0.10 Kb", Number.MemorySize(0x3ea), "#2");
            Assert.AreEqual("4.81 Kb", Number.MemorySize(0x1343), "#3");
            Assert.AreEqual("0.18 Mb", Number.MemorySize(0x0fa343), "#4");
            Assert.AreEqual("99.9 Mb", Number.MemorySize(0x63fa09e), "#5");
            Assert.AreEqual("4.00 Gb", Number.MemorySize(0x100000000), "#6");
            Assert.AreEqual("63.5 Gb", Number.MemorySize(0xfe12fa09e), "#7");
            Assert.AreEqual("643 Pb", Number.MemorySize(0xa0e151936f76553), "#8");
            Assert.AreEqual("888 Pb", Number.MemorySize(0xde1505436f76553), "#9");
            Assert.AreEqual("15.9 Eb", Number.MemorySize(0xffffffffffffffff), "#10");
        }

        [Test]
        public void TestAlign()
        {
            Assert.AreEqual(0x46000, Number.Align(0x45343, 0x1000), "#1");
            Assert.AreEqual(0x45350, Number.Align(0x45343, 0x10), "#2");
        }

        [Test]
        public void TestFlags()
        {
            Assert.AreEqual(6, Number.SingleFlag(0x0040), "#1");
            Assert.AreEqual(7, Number.SingleFlag(0x080), "#2");
            Assert.AreEqual(0, Number.SingleFlag(0x0001), "#3");
            Assert.AreEqual(-1, Number.SingleFlag(0x20001), "#4");
            Assert.AreEqual(-1, Number.SingleFlag(0), "#5");
            Assert.AreEqual(23, Number.SingleFlag(0x800000), "#6");
        }
    }
}
